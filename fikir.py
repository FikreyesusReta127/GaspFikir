from random import randint
right = 0
for _ in range(10):

    num1 = randint(1,10)
    num2 = randint(1,10)

    question = "What is " + str(num1) + " times " + str(num2) + " ? "
    user_answer = int(input(question))
    correct_answer = num1 * num2

    if user_answer == correct_answer:
        print("Thats right, well done!")
        right += 1
    else:
        print("No, im afraid the answer is " + str(correct_answer) + ".")


print("I asked you 10 questions. You got " + str(right) +" of them right.")
print("Well done!")

